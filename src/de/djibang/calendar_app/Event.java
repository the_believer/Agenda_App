package de.djibang.calendar_app;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Event extends CreateJPanel {
static  JPanel pnlEvent;
private JComboBox cbbAccount,cbbEnterpriseName,cbbDate;
private JLabel lblEvent,lblAccount,lblEnterpriseName,lblDate,lblStartWork,lblEndWork,lblWorkingHours,lblNote;
private JTextField txtFldDate, txtFldStartWork, txtFldEndWork, txtFldWorkingHours;  
private JButton previous, done;// later add this button
private JTextArea txtAreaNote;

public Event(){
	buildPanel();
	
}
	@Override
	public JPanel buildPanel() {
		GridBagLayout gbl = new GridBagLayout();
		pnlEvent = new JPanel();
		pnlEvent.setLayout(gbl);
		
		cbbAccount = new JComboBox ();
		cbbEnterpriseName = new JComboBox ();
		cbbDate = new JComboBox ();
		
		lblEvent = new JLabel(" Event ");
		lblAccount= new JLabel(" Account Select ");
		lblEnterpriseName = new JLabel(" Enterprise Select ");
		lblDate = new JLabel(" Date ");
		lblStartWork = new JLabel(" Start Work ");
		lblEndWork = new JLabel(" End Work ");
		lblWorkingHours =new JLabel(" Working Hours ");
		lblNote = new JLabel(" Note ");
		
		txtFldDate = new JTextField();
		txtFldDate.setEditable(false);
		txtFldStartWork =new JTextField();
		txtFldEndWork = new JTextField();
		txtFldWorkingHours = new JTextField();
		txtFldWorkingHours.setEditable(false);
		
		previous = new JButton ("previous");// later change with image
		done = new JButton (" done ");
		
		txtAreaNote = new JTextArea ();
		
		setBounds(pnlEvent,gbl,lblEvent,0,0,2,1,1.0,0.4);
		setBounds(pnlEvent,gbl,lblAccount,0,1,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,cbbAccount,1,1,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblEnterpriseName,0,2,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,cbbEnterpriseName,1,2,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblDate,0,3,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,cbbDate,1,3,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblStartWork,0,4,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,txtFldStartWork,1,4,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblEndWork,0,5,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,txtFldEndWork,1,5,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblWorkingHours,0,6,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,txtFldWorkingHours,1,6,1,1,0.5,0.25);
		setBounds(pnlEvent,gbl,lblNote,0,7,2,1,1,0.25);
		setBounds(pnlEvent,gbl,txtAreaNote,0,8,2,1,1,0.5);
		
		return pnlEvent;
	}
	public void setBounds(JPanel pnl, GridBagLayout gbl, Component cmp,
			               int x, int y, int width, int height,
			               double weightx, double weighty){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = x; gbc.gridy = y;
		gbc.gridwidth = width; gbc.gridheight = height;
		gbc.weightx = weightx; gbc.weighty = weighty;
		gbl.setConstraints(cmp, gbc);
		pnl.add(cmp);
	}

	
}
