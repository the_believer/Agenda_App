package de.djibang.calendar_app;

import javax.swing.JPanel;

public class StartPanelRight extends CreateJPanel {
	static CalendarProgram panel;
	static JPanel pnlRight;
	
	public StartPanelRight(){
		super();
		add(buildPanel());
	}
	
	@Override
	public JPanel buildPanel(){
		new CalendarProgram();
		pnlRight=CalendarProgram.pnlCalendar; 
		return pnlRight;
	}

}
