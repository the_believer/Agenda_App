package de.djibang.calendar_app;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import de.djibang.calendar_app.CalendarProgram;

public class MainWindows {
	
    public static void main(String[] args){
     	SwingUtilities.invokeLater(new Runnable(){
    		public void run(){
    		 new Frame("Calender");	
    		}
    	});
    }
}
 
class Frame extends JFrame {
	static JFrame frame;
	static Stack<JPanel> stack = new Stack<>();
	private static int diffWdth, diffHght;// difference between new Frame width and old
	private static int frameWidth =470;
	private static int frameHeight = 500;
	private static int leftPanelWidth =30;
	private static int newFrameWidth = frameWidth;// initializable but can change see Actionperformed
	private static int newFrameHeight = frameHeight;//same 
	private static int topBottomBorder = 10;
	private static int rightBorder = 5;
	private static int leftBorder = rightBorder;
    
	public Frame(String title){
	    buildFrame(title);
	
	}
	
	public void buildFrame(String title){
		frame = new JFrame();
		new StartPanelleft();
		new StartPanelRight();
		
		
		
		frame.setTitle(title);
		frame.setLayout(new BorderLayout());
		frame.setMinimumSize (new Dimension(frameWidth,frameHeight));
		frame.getRootPane().setBorder(BorderFactory.createMatteBorder(topBottomBorder, leftBorder, topBottomBorder, rightBorder, Color.white));
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(StartPanelRight.pnlRight.getBackground());
		frame.setVisible(true);
      
        
        frame.add(StartPanelleft.westPanel, BorderLayout.WEST);
        frame.add(StartPanelRight.pnlRight, BorderLayout.CENTER);
        stack.push(StartPanelRight.pnlRight);
        
        frame.addComponentListener(new ComponentAdapter(){
        	public void componentResized(ComponentEvent e){
        		Component changedComponent = (Component)e.getSource();
        		
        		
        		diffWdth = changedComponent.getWidth()-newFrameWidth;
        		diffHght = changedComponent.getHeight()-newFrameWidth;
        		newFrameWidth = changedComponent.getWidth();
        		newFrameHeight = changedComponent.getHeight();
        		
        		CalendarProgram.mlblWdth += diffWdth;
        		//panel.tblHght +=diffHght;
        		//panel.rowHght += diffHght/100;
        		//panel.setRowHeight();
        		CalendarProgram.setBounds();
        		//frame.repaint();
        		
        		
        		Registration.usernameWdth += diffWdth/2;
        		Registration.setBounds();
        		
        	}
        	
        });
        
	}	
		
}

