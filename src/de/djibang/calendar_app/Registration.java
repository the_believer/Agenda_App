package de.djibang.calendar_app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.persistence.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

//@Entity
//@Table(name = "registration_db")
class Registration extends CreateJPanel {

	static SessionFactory factory;

	// @Id @GeneratedValue
	// @Column (name ="id")
	static int id ;
	// @Column (name = "tax_class")
	static int taxClass;
	// @Column (name = "statut")
	static int statut;
	// @Column (name = "username")
	static String username;
	// @Column (name = "name_of_enterprise")
	static String nameOfEnterprise;

	static JPanel regPanel, pnlStep1, pnlStep2;
	static JLabel taxCategory, lblStdt;
	static JTextField usernameStep1, entNameStep2;
	static int lftBorder = 10;
	static int topBorder = 30;
	static int usernameWdth = 250;
	static int usernameHght = 25;
	static JCheckBox taxCategory1;
	static JCheckBox taxCategory2;
	static JCheckBox taxCategory3;
	static JCheckBox taxCategory4;
	static JCheckBox taxCategory5;
	static JCheckBox taxCategory6;
	static JCheckBox lblStdtJa;
	static JCheckBox lblStdtNein;
	static JButton submit;
	int hgap = 2;
	int vgap = 1;

	public Registration() {
		super();
		add(buildPanel());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		Registration.id = id;
	}

	public String getusername() {
		return Registration.username;
	}

	public void setusername(String user_name) {
		Registration.username = user_name;
	}

	public String getnameOfEnterprise() {
		return Registration.nameOfEnterprise;
	}

	public void setnameOfEnterprise(String name_of_enterprise) {
		Registration.nameOfEnterprise = name_of_enterprise;
	}

	public int getstatut() {
		return Registration.statut;
	}

	public void setstatut(int statut) {
		Registration.statut = statut;
	}

	public int gettaxClass() {
		return Registration.taxClass;
	}

	public void settaxClass(int tax_class) {
		Registration.taxClass = tax_class;
	}

	@Override
	public JPanel buildPanel() {
		// lblStep1 = new JLabel ("Step1 : your identity");
		// lblStep2 = new JLabel ("Step2 : Information about Job");
		taxCategory = new JLabel("Tax class");
		usernameStep1 = new JTextField("Username");
		entNameStep2 = new JTextField("name of Entreprise");
		taxCategory1 = new JCheckBox(" 1 ");
		taxCategory1.setOpaque(false);
		taxCategory2 = new JCheckBox(" 2 ");
		taxCategory2.setOpaque(false);
		taxCategory3 = new JCheckBox(" 3 ");
		taxCategory3.setOpaque(false);
		taxCategory4 = new JCheckBox(" 4 ");
		taxCategory4.setOpaque(false);
		taxCategory5 = new JCheckBox(" 5 ");
		taxCategory5.setOpaque(false);
		taxCategory6 = new JCheckBox(" 6 ");
		taxCategory6.setOpaque(false);
		lblStdt = new JLabel(" Student ");
		lblStdtJa = new JCheckBox(" Yes ");
		lblStdtJa.setOpaque(false);
		lblStdtNein = new JCheckBox(" No");
		lblStdtNein.setOpaque(false);
		submit = new JButton("submit");

		// add controll or listener
		submit.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent event) {
				convertSwingToStandarDatatype();

				// try to create sessionFactory object
				try {
					factory = new Configuration().configure().buildSessionFactory();
				} catch (Throwable ex) {
					System.err.println("Failed to create sessionFactory object." + ex);
					throw new ExceptionInInitializerError(ex);
				}
				addAccountToDb();
			}

		});

		regPanel = new JPanel();
		// regPanel.setSize(350, 350);
		regPanel.setBackground(new Color(249, 190, 71));
		regPanel.setLayout(new GridLayout(hgap, vgap));

		pnlStep1 = new JPanel();
		// pnlStep1.setSize(180,180);
		pnlStep1.setOpaque(false);
		// pnlStep1.setBackground(Color.gray);
		pnlStep1.setLayout(null);
		pnlStep1.setBorder(BorderFactory.createTitledBorder("Step1 : your identity"));

		pnlStep2 = new JPanel();
		// pnlStep2.setSize(160, 160);
		pnlStep2.setOpaque(false);
		pnlStep2.setLayout(null);
		pnlStep2.setBorder(BorderFactory.createTitledBorder("Step2 : Information about Job"));

		// Add control to pnlStep1 and pnlStep2
		pnlStep1.add(usernameStep1);
		pnlStep2.add(taxCategory);
		pnlStep2.add(entNameStep2);
		pnlStep2.add(taxCategory1);
		pnlStep2.add(taxCategory2);
		pnlStep2.add(taxCategory3);
		pnlStep2.add(taxCategory4);
		pnlStep2.add(taxCategory5);
		pnlStep2.add(taxCategory6);
		pnlStep2.add(lblStdt);
		pnlStep2.add(lblStdtJa);
		pnlStep2.add(lblStdtNein);
		pnlStep2.add(submit);

		setBounds();

		// add control to regPanel
		regPanel.add(pnlStep1);
		regPanel.add(pnlStep2);

		return regPanel;
	}

	static public void setBounds() {
		// setBounds
		usernameStep1.setBounds(lftBorder, topBorder, usernameWdth, usernameHght);
		entNameStep2.setBounds(lftBorder, topBorder, usernameWdth, usernameHght);
		lblStdt.setBounds(lftBorder, topBorder + usernameHght, usernameWdth, usernameHght);
		lblStdtJa.setBounds(lftBorder, topBorder + (usernameHght * 2), usernameWdth / 2, usernameHght);
		lblStdtNein.setBounds(lftBorder + (usernameWdth / 2), topBorder + (usernameHght * 2), usernameWdth / 2,
				usernameHght);
		taxCategory.setBounds(lftBorder, topBorder + (usernameHght * 3), usernameWdth, usernameHght);
		taxCategory1.setBounds(lftBorder, topBorder + (usernameHght * 4), usernameWdth / 6, usernameHght);
		taxCategory2.setBounds(lftBorder + (usernameWdth / 6), topBorder + (usernameHght * 4), usernameWdth / 6,
				usernameHght);
		taxCategory3.setBounds(lftBorder + (usernameWdth / 6 * 2), topBorder + (usernameHght * 4), usernameWdth / 6,
				usernameHght);
		taxCategory4.setBounds(lftBorder + (usernameWdth / 6 * 3), topBorder + (usernameHght * 4), usernameWdth / 6,
				usernameHght);
		taxCategory5.setBounds(lftBorder + (usernameWdth / 6 * 4), topBorder + (usernameHght * 4), usernameWdth / 6,
				usernameHght);
		taxCategory6.setBounds(lftBorder + (usernameWdth / 6 * 5), topBorder + (usernameHght * 4), usernameWdth / 6,
				usernameHght);
		submit.setBounds(lftBorder, topBorder + (usernameHght * 6), usernameWdth / 3, usernameHght);

	}

	static public void convertSwingToStandarDatatype() {
		username = usernameStep1.getText();
		nameOfEnterprise = entNameStep2.getText();

		if (lblStdtJa.isSelected() == true)
			statut = 1;
		else if (lblStdtNein.isSelected() == true)
			statut = 0;

		if (taxCategory1.isSelected() == true)
			taxClass = 1;
		else if (taxCategory2.isSelected() == true)
			taxClass = 2;
		else if (taxCategory3.isSelected() == true)
			taxClass = 3;
		else if (taxCategory4.isSelected() == true)
			taxClass = 4;
		else if (taxCategory5.isSelected() == true)
			taxClass = 5;
		else if (taxCategory6.isSelected() == true)
			taxClass = 6;

	}

	static public void addAccountToDb() {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
            System.out.println("Transaction begin");
			session.save(new Registration());
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
