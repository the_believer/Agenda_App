package de.djibang.calendar_app;

import javax.swing.JPanel;

public abstract class CreateJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public abstract JPanel buildPanel();
}
