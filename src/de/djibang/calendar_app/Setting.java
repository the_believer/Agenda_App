package de.djibang.calendar_app;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Setting extends CreateJPanel {
static JPanel pnlSetting;
private JLabel lblSetting,lblUserName, lblTaxCtg;
private static JComboBox cmbUserName, cmbTaxCtg;

public Setting(){
	buildPanel();
}

	@Override
	public JPanel buildPanel() {
		GridBagLayout gbl = new GridBagLayout();
	pnlSetting = new JPanel();
	pnlSetting.setLayout(gbl);
	lblSetting = new JLabel(" Setting ");
	lblUserName = new JLabel(" Select Username");
	lblTaxCtg = new JLabel ("Change Tax Category");
	cmbUserName = new JComboBox ();
	cmbTaxCtg = new JComboBox();
	
	setBounds(pnlSetting,gbl,lblSetting,0,0,2,1,1.0,0.4);
	setBounds(pnlSetting,gbl,lblUserName,0,1,1,1,0.5,0.25);
	setBounds(pnlSetting,gbl,cmbUserName,1,1,1,1,0.5,0.25);
	setBounds(pnlSetting,gbl,lblTaxCtg,0,2,1,1,0.5,0.25);
	setBounds(pnlSetting,gbl,cmbTaxCtg,1,2,1,1,0.5,0.25);
	
		return pnlSetting;
	}
	
	public void setBounds(JPanel pnl, GridBagLayout gbl, Component cmp,
            int x, int y, int width, int height,
            double weightx, double weighty){
GridBagConstraints gbc = new GridBagConstraints();
gbc.fill = GridBagConstraints.BOTH;
gbc.gridx = x; gbc.gridy = y;
gbc.gridwidth = width; gbc.gridheight = height;
gbc.weightx = weightx; gbc.weighty = weighty;
gbl.setConstraints(cmp, gbc);
pnl.add(cmp);
}

}
