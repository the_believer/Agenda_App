package de.djibang.calendar_app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class StartPanelleft extends CreateJPanel  {
	private String buttonName11 = "Registration";
    private String buttonName21 = "timeline";
    private String buttonName31 = "to do";
    private String buttonName41 = "Setting";
    static  JPanel westPanel; 

	public StartPanelleft (){
		super();
		add(buildPanel());
	}
    
	@Override
	public JPanel buildPanel(){
		new Registration();
		new Setting();
		
		westPanel = new JPanel();
		westPanel.setLayout(new GridLayout(4,1));
		westPanel.setBackground(Color.white);
		//westPanel.setMinimumSize(new Dimension(leftPanelWidth, newFrameHeight) );
		
		
		
		JButton button11 = new JButton();
		button11.addActionListener(event -> {          //lambdas only Java 8
			Frame.frame.remove(StartPanelRight.pnlRight);
			Frame.frame.remove(Event.pnlEvent);
			Frame.frame.remove(Setting.pnlSetting);
			Frame.frame.add(Registration.regPanel);
			Frame.frame.validate();
			Frame.frame.repaint();			
		});
		
		TextIcon t1 = new TextIcon(button11, buttonName11, TextIcon.Layout.HORIZONTAL);
		RotatedIcon r1 = new RotatedIcon(t1, RotatedIcon.Rotate.UP);
		button11.setIcon( r1 );
		button11.setForeground(Color.white);
		button11.setBackground(new Color(249,190,71));
		
		JButton button21 = new JButton();
		
		button21.addActionListener(event -> {
			Frame.frame.remove(Registration.regPanel);
			Frame.frame.remove(Event.pnlEvent);
			Frame.frame.remove(Setting.pnlSetting);
			Frame.frame.add(StartPanelRight.pnlRight);
			Frame.frame.validate();
			Frame.frame.repaint();
		});
		
		TextIcon t2 = new TextIcon(button21, buttonName21, TextIcon.Layout.HORIZONTAL);
		RotatedIcon r2 = new RotatedIcon(t2, RotatedIcon.Rotate.UP);
		button21.setIcon( r2 );
		button21.setForeground(Color.white);
		button21.setBackground(new Color(30,195,216));
		
		JButton button31 = new JButton();
		TextIcon t3 = new TextIcon(button11, buttonName31, TextIcon.Layout.HORIZONTAL);
		RotatedIcon r3 = new RotatedIcon(t3, RotatedIcon.Rotate.UP);
		button31.setIcon( r3 );
		button31.setForeground(Color.white);
		button31.setBackground(new Color(216,111,30));
		
		JButton button41 = new JButton();
		button41.addActionListener(event -> {          //lambdas only Java 8
			Frame.frame.remove(StartPanelRight.pnlRight);
			Frame.frame.remove(Event.pnlEvent);
			Frame.frame.remove(Registration.regPanel);
			Frame.frame.add(Setting.pnlSetting);
			Frame.frame.validate();
			Frame.frame.repaint();			
		});
		TextIcon t4 = new TextIcon(button41, buttonName41, TextIcon.Layout.HORIZONTAL);
		RotatedIcon r4 = new RotatedIcon(t4, RotatedIcon.Rotate.UP);
		button41.setIcon( r4 );
		button41.setForeground(Color.white);
		button41.setBackground(new Color(35,188,58));
		
		westPanel.add(button11);
		westPanel.add(button21);
		westPanel.add(button31);
		westPanel.add(button41);
		
		return westPanel;
	}
}
